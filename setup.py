# This file relies on https://github.com/pypa/sampleproject
# as a draft implementation that is 'pretty good' to start with.

from setuptools import setup, find_packages

PROJECT_DESCRIPTION = "pyIonosphere is a Python3 package (library) \
 of the Earth's ionosphere models."

PROJECT_DESCRIPTION_LONG = """
pyIonosphere is a Python3 package (library) of the Earth's ionosphere models.

It contains:

 - pyfiri - FIRI-2018 non-disturbed non-auroral ionosphere D-region model;

 - pydanilov - Danilov non-disturbed non-auroral ionosphere D-region model;

 - pyimaz - IMAZ non-disturbed high-latitude ionosphere D-region model. 
"""

setup(
    # this will be the package name you will see, e.g. the output of 'conda list' in anaconda prompt
    name='pyionosphere',

    # Versions should comply with PEP 440, https://www.python.org/dev/peps/pep-0440/
    version='0.0.3b2',
    description=PROJECT_DESCRIPTION,
    long_description=PROJECT_DESCRIPTION_LONG,
    # long_description_content_type='text/markdown', - some bug with markdown - displayed not correctly
    url='https://gitlab.com/zolotov/pyionosphere/',  # homepage
    license='Apache Software License',  # to remove 'license: UNKNOWN' notice
    platform=['any'],
    author='Oleg Zolotov, Denis Sakaev',
    author_email='zolotovo@gmail.com',

    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        'Development Status :: 1 - Planning',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics',

        # License
        'License :: OSI Approved :: Apache Software License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate you support Python 3. These classifiers are *not*
        # checked by 'pip install'.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3 :: Only',
    ],

    keywords='ionosphere of the Earth, model, pyIonosphere',

    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    #
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=["numpy", "scipy", "xarray", "pyfiri", "pydanilov", "pyimaz"],  # Optional

    # Automatically finds out all directories (packages) - those must contain a file named __init__.py (can be empty)
    packages=find_packages()
    #,  # include/exclude arguments take * as wildcard, . for any sub-package names

    #package_data={'pyimaz': [r'_ext/_data/chapman.txt',r'_ext/_data/data biases.txt',r'_ext/_data/data weights.txt',r'_ext/_data/Imaz pressure.txt',r'_ext/_data/nighttruequiet.txt',r'_ext/_data/press_60deg.txt',r'_ext/_data/press_70deg.txt']},
    #include_package_data=True
)

# To generate wheel, a command for the terminal:
# python setup.py bdist_wheel
# twine upload --repository testpypi dist/*
